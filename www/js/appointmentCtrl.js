app.controller('AppointmentCtrl', function($scope, Cities, PormotionsOffers, Appointment, $filter, CityBranchId, $state, $stateParams, localStorageService, $rootScope, $ionicLoading, $ionicModal, $ionicPopup) {
    console.log("Chalgaya");

    var cities = localStorageService.get('cities')
    $scope.cities = {
        selectedOption: { CityId: 1, CityName: "Riyadh" },
        availableOptions: cities
    }
    // getting cities through api//
    $scope.getCities = [];
    $scope.cityId = "";
    $scope.getCityId = function() {
        console.log($scope.cityId)
    }
    // PormotionsOffers.gettCities().success(function(res) {
    PormotionsOffers.gettCities().then(function(res) {
            for (var i = 0; i < res.length; i++) {
                $scope.getCities.push({
                    CityId: res[i].CityId,
                    CityName: res[i].CityName,

                })
            }

        // })
        // .error(function(err) {
        }, function(err) {
            // err=  err.data
            console.log(err);
        })

    $scope.selectCity = function(index) {
        for (var i = 0; i < $scope.getCities.length; i++) {
            if (i == index) {
                $scope.getCities[i].isChecked = true;
            } else {
                $scope.getCities[i].isChecked = false;
            }

        }

    }
    $scope.branches = {};
    $ionicLoading.show();




    //Default Calling for Branches
    // Appointment.getBranches($scope.cities.selectedOption.CityId).success(function(res) {
    Appointment.getBranches($scope.cities.selectedOption.CityId).then(function(res) {
            console.log(res);
            // $ionicLoading.hide();
            $scope.branches = {
                selectedOption: { Id: res[0].Id, BranchName: res[0].BranchName },
                availableOptions: res
            };

            //Default Calling for Services
            // Appointment.getServices(res[0].Id).success(function(res) {
            Appointment.getServices(res[0].Id).then(function(res) {
                    $ionicLoading.hide();
                    $scope.services = {
                        selectedOption: { Id: res[0].Id, Title_Eng: res[0].Title_Eng },
                        availableOptions: res
                    };
                // })
                // .error(function(err) {
                }, function(err) {
                    // err=  err.data
                    $ionicLoading.hide();
                })
        // })
        // .error(function(err) {
        }, function(err) {
            // err=  err.data
            console.log(err);
            $ionicLoading.hide();
        })
    $scope.openCitiesPicker = function() {
        $scope.modal.show();
    }

    $ionicModal.fromTemplateUrl('templates/citiesModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.closemodal = function() {
        $scope.modal.hide();
    }

    $scope.hasChanged = function() {
        console.log("branch changed", $scope.cities.selectedOption)
        $ionicLoading.show();
        // Appointment.getBranches($scope.cities.selectedOption.CityId).success(function(res) {
        Appointment.getBranches($scope.cities.selectedOption.CityId).then(function(res) {
                console.log(res);
                $scope.branches = {
                    selectedOption: { Id: res[0].Id, BranchName: res[0].BranchName },
                    availableOptions: res
                };

                //Default Calling for Services
                // Appointment.getServices(res[0].Id).success(function(res) {
                Appointment.getServices(res[0].Id).then(function(res) {
                        if (res.length > 0) {
                            $ionicLoading.hide();
                            $scope.services = {
                                selectedOption: { Id: res[0].Id, Title_Eng: res[0].Title_Eng },
                                availableOptions: res
                            };
                        } else {
                            $ionicLoading.hide();
                            $scope.services = {}
                        }
                    // })
                    // .error(function(err) {
                    }, function(err) {
                        // err=  err.data
                        $ionicLoading.hide();
                    })


            // })
            // .error(function(err) {
            }, function(err) {
                // err=  err.data
                console.log(err);
                $ionicLoading.hide();
            })
    }


    $scope.branchChanged = function() {
        $ionicLoading.show();
        // Appointment.getServices($scope.branches.selectedOption.Id).success(function(res) {
        Appointment.getServices($scope.branches.selectedOption.Id).then(function(res) {
            $ionicLoading.hide();
            if (res[0] != null) {
                $scope.services = {
                    selectedOption: { Id: res[0].Id, Title_Eng: res[0].Title_Eng },
                    availableOptions: res
                }
            } else {
                $scope.services = {};
            }
        }, function(err) {
            // err=  err.data
        })

    }



    $scope.next = function() {


        try {
            CityBranchId.set_cityid($scope.cities.selectedOption.CityId);
            CityBranchId.set_branchid($scope.branches.selectedOption.Id);
            CityBranchId.set_serviceid($scope.services.selectedOption.Id);
            localStorageService.set("bookingParams", {
                CityId: $scope.cities.selectedOption.CityId,
                BranchId: $scope.branches.selectedOption.Id,
                ServiceId: $scope.services.selectedOption.Id,
            })
            console.log("cities", $scope.cities.selectedOption.CityId);
            console.log("branches", $scope.branches.selectedOption.Id)
            console.log("services", $scope.services.selectedOption.Id)
            $state.go('app.bookappointment', {reload: true});
        } catch (err) {
            var alertPopup = $ionicPopup.alert({
                title: 'Error',
                template: 'No services available in selected branch'
            });

            alertPopup.then(function(res) {
                // Custom functionality....
            });
        }

        //$rootScope.navigate('app.bookappointment', { branchid: $scope.branches.selectedOption.Id , serviceid : $scope.branches.selectedOption.Id})
        //$state.go('app.bookappointment', { reload: true });
        // $scope.cities = Cities.cities;
    }



    $scope.disabledDates = []

    // Appointment.getAnnualHolidayDays().success(function(res) {
    Appointment.getAnnualHolidayDays().then(function(res) {
        console.log("resss",res)
        for (var i = 0; i < res.length; i++) {
            var currentDate = new Date(res[i].StartDate);
            var endDate = new Date(res[i].EndDate);
            console.log("start date", currentDate, endDate)
            var j = 0;
            if (currentDate == endDate) {
                j = j + 1;
                $scope.disabledDates.push(new Date($filter("date")(currentDate, 'yyyy/MM/dd')));
            } else {

                while (currentDate <= endDate) {
                    
                    currentDate = new Date(currentDate.setDate(currentDate.getDate() + 1));
                    $scope.disabledDates.push(new Date($filter("date")(currentDate, 'yyyy/MM/dd')));
                    j = j + 1;
                    // console.log((j) + (i) + '->' + $filter("date")(currentDate, 'dd-MM-yyyy'));
                }
            }
        }

        localStorageService.set("disabledDates", $scope.disabledDates);

    }, function(err) {
        // err=  err.data
    });



})