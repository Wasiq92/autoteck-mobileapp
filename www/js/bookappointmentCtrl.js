app.controller('BookAppointmentCtrl', function($scope, $filter, $state, $http, $ionicModal, $ionicPopup, CityBranchId, Appointment, ionicTimePicker, $stateParams, AppointmentDetail, $rootScope, localStorageService, $ionicLoading) {

    console.log("APPOINTMENT CONTROL");

    $scope.branchWorkingDays = [];
    $scope.final_Obj = localStorageService.get("bookingParams");
    //    $ionicLoading.show();
    var current_date = new Date();
    $scope.disabledDates = [];
    var weekDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    $scope.branchWorkingDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    //Disable Calender Dates;
    $scope.disabledDates = localStorageService.get("disabledDates");
    // Appointment.getBranchWorkingDays($scope.final_Obj.BranchId).success(function(res) {
    //     console.log("Brach Days ", res);
    //     //$scope.branchWorkingDays = res;
    //     $ionicLoading.hide();
    // })
    // .error(function(err) {
    //     $ionicLoading.hide();
    // })



    //////////////////// LOGIC
    var d = new Date();
    var n = d.getDay();

    $scope.disabledDates = $scope.disabledDates.concat(getDates(new Date(2016, 3, 18), current_date.subDays(1)));
    $scope.disabledDates = $scope.disabledDates.concat(getFridays(new Date(), new Date('2022-04-29T00:00:00.000Z')));
    $scope.disabledDates = $scope.disabledDates.concat(getSpecificDay(new Date(), new Date('2022-04-29T00:00:00.000Z'), 6));
    $scope.disabledDates = $scope.disabledDates.concat(getSpecificDay(new Date(), new Date('2022-04-29T00:00:00.000Z'), 0));


    if (((new Date()).getMonth() + 2) > 11) {
        if (((new Date()).getMonth() + 2) == 12) {

            $scope.dateMax = ((new Date()).getFullYear() + 1) + '-' + '0' + '-1';
        }

        if (((new Date()).getMonth() + 2) == 13) {
            $scope.dateMax = ((new Date()).getFullYear() + 1) + '-' + '1' + '-1';
        }
    } else {
        $scope.dateMax = (new Date()).getFullYear() + '-' + ((new Date()).getMonth() + 2) + '-1';
    }



    function getDates(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
            dateArray.push(new Date(currentDate))
            currentDate = currentDate.addDays(1);
        }
        return dateArray;
    }


    function getFridays(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
            if (currentDate.getDay() == 5) {
                dateArray.push($filter('date')(new Date(currentDate), "yyyy-MM-ddT00:00:00.000"))
            }
            //dateArray.push(currentDate)
            currentDate = currentDate.addDays(1);
        }
        return dateArray;
    }


    function getSpecificDay(startDate, stopDate, dayValue) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
            if (currentDate.getDay() == dayValue) {
                dateArray.push($filter('date')(new Date(currentDate), "yyyy-MM-ddT00:00:00.000"))
            }
            //dateArray.push(currentDate)
            currentDate = currentDate.addDays(1);
        }
        return dateArray;
    }


    console.log("date max ---------", $scope.dateMax);
    $scope.dateobj = {};
    $rootScope.$on('ERRORAVAILABLESLOTS', function(event, args) {
        $scope.dateobj.date = current_date;
        $scope.hours = 00;
        $scope.minutes = 00;
        $scope.ampm = "AM";
        //console.log(date);
        var getdate = current_date.getDate()
        var month = current_date.getMonth();
        var year = current_date.getFullYear();
        $scope.appointdate = undefined;
        $scope.starttime = undefined
        $scope.endtime = undefined
    })
    $scope.dateobj.date = current_date;
    $scope.hours = 00;
    $scope.minutes = 00;
    $scope.ampm = "AM";
    //console.log(date);
    var getdate = current_date.getDate()
    var month = current_date.getMonth();
    var year = current_date.getFullYear();
    var branchid = $stateParams.branchid;
    //$ionicLoading.show();

    // Appointment.disableHolidays().success(function(res) {
    //     console.log("disable holidays", res)
    // })
    // .error(function(err) {
    //     console.log("Errr",err)
    // })
    // Appointment.getAvailableDays(branchid, year, month + 1).success(function(res) {
    //         console.log(res);
    //         Appointment.getAvailableSlots(branchid, year, month + 1, getdate).success(function(result) {
    //                 console.log(result)
    //                 $rootScope.$broadcast('AVAILABLEDAYS', { data: result });
    //                 $ionicLoading.hide();
    //             })
    //             .error(function(error) {
    //                 console.log(error)
    //                 $ionicLoading.hide();
    //             })
    //     })
    //     .error(function(err) {
    //         console.log(err)
    //         $ionicLoading.hide();
    //     })

    var ipObj1 = {
        callback: function(val) { //Mandatory
            console.log(val)
            if (typeof(val) === 'undefined') {
                console.log('Time not selected');
            } else {
                var selectedTime = new Date(val * 1000);
                console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
                var hh = selectedTime.getUTCHours();
                var h = hh;
                dd = "AM";
                if (h >= 12) {
                    h = hh - 12;
                    dd = "PM";
                }
                if (h == 0) {
                    h = 12;
                }
                $scope.hours = h < 10 ? ("0" + h) : h;
                var mm = selectedTime.getUTCMinutes();
                $scope.minutes = mm < 10 ? ("0" + mm) : mm;
                $scope.ampm = dd;
            }
        },
        inputTime: 50400, //Optional
        format: 12, //Optional
        step: 1, //Optional
        setLabel: 'Set' //Optional
    };

    $scope.openTimePicker = function() {
        $scope.modal.show();
    }

    $ionicModal.fromTemplateUrl('templates/timemodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $rootScope.$on('AVAILABLESLOTS', function(event, args) {
        console.log("args ", args)
        // for(var i =0 ; i < args.length ; i++){
        //      $scope.Data = 
        // }
        $scope.appointments = args;



        // $scope.selectedOption = $scope.options[1];
        $scope.timeslots = [];
        if (args.data != "OffDay") {
            for (var i = 0; i < args.data.length; i++) {
                $scope.timeslots.push(args.data[i]);
                $scope.timeslots[i].isChecked = false;
            }
            $scope.timeslots[0].isChecked = true;
            $scope.hours = $scope.timeslots[0].StartTimeStr.substr(0, 2);
            $scope.minutes = $scope.timeslots[0].StartTimeStr.substr(3, 2);
            $scope.ampm = $scope.timeslots[0].StartTimeStr.substr(6, 2);
            $scope.appointdate = $scope.timeslots[0].AppointmentDate.substr(0, 10);
            $scope.starttime = $scope.timeslots[0].StartTime.substr(0, 10) + " " + $scope.timeslots[i - 1].StartTimeStr;
            $scope.endtime = $scope.timeslots[0].EndTime.substr(0, 10) + " " + $scope.timeslots[i - 1].EndTimeStr;
        }


    })

    $scope.closemodal = function() {
        $scope.modal.hide();
        for (var i = 0; i < $scope.timeslots.length; i++) {
            if ($scope.timeslots[i].isChecked) {
                $scope.hours = $scope.timeslots[i].StartTimeStr.substr(0, 2);
                $scope.minutes = $scope.timeslots[i].StartTimeStr.substr(3, 2);
                $scope.ampm = $scope.timeslots[i].StartTimeStr.substr(6, 2);
                $scope.appointdate = $scope.timeslots[i].AppointmentDate.substr(0, 10);
                $scope.starttime = $scope.timeslots[i].StartTime.substr(0, 10) + " " + $scope.timeslots[i].StartTimeStr;
                $scope.endtime = $scope.timeslots[i].EndTime.substr(0, 10) + " " + $scope.timeslots[i].EndTimeStr;
            }
        }
    }

    $scope.selectTime = function(index) {
        console.log("index", index)
        for (var i = 0; i < $scope.timeslots.length; i++) {
            if (i == index) {
                $scope.timeslots[i].isChecked = true;
            } else {
                $scope.timeslots[i].isChecked = false;
            }

        }

    }
    $scope.final_obj = {};
    $scope.book = function() {
        console.log("scope.date", $scope.dateobj.date)
        console.log("AppointmentSlot", {
            BranchId: $scope.final_Obj.BranchId,
            AppointmentDate: $scope.appointdate,
            StartTime: $scope.starttime,
            EndTime: $scope.endtime
        })

        if (typeof $scope.appointdate == "undefined") {
            $scope.showAlert(false);
        } else {
            $scope.final_obj.AppointmentSlot = {
                BranchId: $scope.final_Obj.BranchId,
                AppointmentDate: $scope.appointdate,
                StartTime: $scope.starttime,
                EndTime: $scope.endtime
            }
            $scope.final_obj.TypeOfService = $scope.final_Obj.ServiceId;
            $scope.final_obj.CustomerVehicleId = 0;
            $ionicLoading.show();
            // Appointment.schedule($scope.final_obj).success(function(res) {
            Appointment.schedule($scope.final_obj).then(function(res) {
                    $ionicLoading.hide();
                    $scope.showAlert(true);
                // })
                // .error(function(err) {
                }, function(err) {
                    // err=  err.data
                    $ionicLoading.hide();
                    $scope.showAlert(false);
                })
        }
    }

    // An alert dialog
    $scope.showAlert = function(check) {
        var alertPopup = $ionicPopup.alert({
            title: check == true ? 'Success!' : 'Error!',
            template: check == true ? 'Your appointment has been scheduled!' : 'Appointment slot is not available.'
        });

        alertPopup.then(function(res) {
            console.log('Thank you for not eating my delicious ice cream cone');
            if (check) {
                $rootScope.navigate('main')
            }
        });
    };


})